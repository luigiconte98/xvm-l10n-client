msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Polibsharp 2.0\n"

#: Warning
msgctxt "Warning"
msgid "Warning"
msgstr "تحذير"

#: Error
msgctxt "Error"
msgid "Error"
msgstr "خطأ"

#: Information
msgctxt "Information"
msgid "Information"
msgstr "معلومات"

#: OK
msgctxt "OK"
msgid "OK"
msgstr "موافق"

#: Cancel
msgctxt "Cancel"
msgid "Cancel"
msgstr "إلغاء"

#: Save
msgctxt "Save"
msgid "Save"
msgstr "حفظ"

#: Remove
msgctxt "Remove"
msgid "Remove"
msgstr "إزالة"

#: Yes
msgctxt "Yes"
msgid "Yes"
msgstr "نعم"

#: No
msgctxt "No"
msgid "No"
msgstr "لا"

#: Not available short
msgctxt "Not available short"
msgid "n/a"
msgstr "غير معيّن"

#: from
msgctxt "from"
msgid "from"
msgstr "من"

#: m
msgctxt "m"
msgid "m"
msgstr ""

#: Initialization
msgctxt "Initialization"
msgid "Initialization"
msgstr "جارِ التهيئة..."

#: Ping
msgctxt "Ping"
msgid "Ping"
msgstr "بِنْج"

#: Online
msgctxt "Online"
msgid "Online"
msgstr "متصل"

#: shot
msgctxt "shot"
msgid "shot"
msgstr ""

#: fire
msgctxt "fire"
msgid "fire"
msgstr ""

#: ramming
msgctxt "ramming"
msgid "ramming"
msgstr ""

#: world_collision
msgctxt "world_collision"
msgid "falling"
msgstr ""

#: death_zone
msgctxt "death_zone"
msgid "death zone"
msgstr ""

#: pillbox
msgctxt "pillbox"
msgid "Pillboxes {0}"
msgstr ""

#: Hits
msgctxt "Hits"
msgid "Hits"
msgstr ""

#: Total
msgctxt "Total"
msgid "Total"
msgstr ""

#: Last
msgctxt "Last"
msgid "Last"
msgstr ""

#: enemyBaseCapture
msgctxt "enemyBaseCapture"
msgid "Base{0} capture by allies!"
msgstr ""

#: enemyBaseCaptured
msgctxt "enemyBaseCaptured"
msgid "Base{0} captured by allies!"
msgstr ""

#: allyBaseCapture
msgctxt "allyBaseCapture"
msgid "Base{0} capture by enemies!"
msgstr ""

#: allyBaseCaptured
msgctxt "allyBaseCaptured"
msgid "Base{0} captured by enemies!"
msgstr ""

#: avgDamage
msgctxt "avgDamage"
msgid "Avg damage"
msgstr ""

#: mainGun
msgctxt "mainGun"
msgid "High caliber"
msgstr ""

#: engine
msgctxt "engine"
msgid "engine"
msgstr ""

#: ammo_bay
msgctxt "ammo_bay"
msgid "ammo bay"
msgstr ""

#: fuel_tank
msgctxt "fuel_tank"
msgid "fuel tank"
msgstr ""

#: radio
msgctxt "radio"
msgid "radio"
msgstr ""

#: left_track
msgctxt "left_track"
msgid "left track"
msgstr ""

#: right_track
msgctxt "right_track"
msgid "right track"
msgstr ""

#: gun
msgctxt "gun"
msgid "gun"
msgstr ""

#: turret_rotator
msgctxt "turret_rotator"
msgid "turret rotator"
msgstr ""

#: surveying_device
msgctxt "surveying_device"
msgid "surveying device"
msgstr ""

#: commander
msgctxt "commander"
msgid "commander"
msgstr ""

#: driver
msgctxt "driver"
msgid "driver"
msgstr ""

#: radioman
msgctxt "radioman"
msgid "radioman"
msgstr ""

#: gunner
msgctxt "gunner"
msgid "gunner"
msgstr ""

#: loader
msgctxt "loader"
msgid "loader"
msgstr ""

#: sec
msgctxt "sec"
msgid "sec"
msgstr ""

#: drowning
msgctxt "drowning"
msgid "drowning"
msgstr "تحذير"

#: forsaken
msgctxt "forsaken"
msgid "forsaken"
msgstr ""

#: fireMsg
msgctxt "fireMsg"
msgid "FIRE"
msgstr ""

#: armor_piercing
msgctxt "armor_piercing"
msgid "AP"
msgstr ""

#: high_explosive
msgctxt "high_explosive"
msgid "HE"
msgstr ""

#: armor_piercing_cr
msgctxt "armor_piercing_cr"
msgid "APCR"
msgstr ""

#: armor_piercing_he
msgctxt "armor_piercing_he"
msgid "HESH"
msgstr ""

#: hollow_charge
msgctxt "hollow_charge"
msgid "HEAT"
msgstr ""

#: intermediate_ricochet
msgctxt "intermediate_ricochet"
msgid "ricochet"
msgstr ""

#: final_ricochet
msgctxt "final_ricochet"
msgid "ricochet"
msgstr ""

#: armor_not_pierced
msgctxt "armor_not_pierced"
msgid "bounce"
msgstr ""

#: armor_pierced_no_damage
msgctxt "armor_pierced_no_damage"
msgid "blocked"
msgstr ""

#: turret
msgctxt "turret"
msgid "turret"
msgstr ""

#: hull
msgctxt "hull"
msgid "hull"
msgstr ""

#: chassis
msgctxt "chassis"
msgid "chassis"
msgstr ""

#: wheel
msgctxt "wheel"
msgid "wheel"
msgstr ""

#: Hit percent
msgctxt "Hit percent"
msgid "Hit percent"
msgstr ""

#: Damage (assisted / own)
msgctxt "Damage (assisted / own)"
msgid "Damage (assisted / own)"
msgstr ""

#: BR_xpCrew
msgctxt "BR_xpCrew"
msgid "<font face='xvm'>&#x6B;</font>"
msgstr ""

#: Friend
msgctxt "Friend"
msgid "Friend"
msgstr ""

#: Ignored
msgctxt "Ignored"
msgid "Ignored"
msgstr ""

#: unknown
msgctxt "unknown"
msgid "unknown"
msgstr ""

#: Fights
msgctxt "Fights"
msgid "Fights"
msgstr ""

#: Wins
msgctxt "Wins"
msgid "Wins"
msgstr ""

#: Data was updated at
msgctxt "Data was updated at"
msgid "Data was updated at"
msgstr ""

#: Load statistics
msgctxt "Load statistics"
msgid "Load statistics"
msgstr ""

#: General stats
msgctxt "General stats"
msgid "General stats"
msgstr ""

#: Summary
msgctxt "Summary"
msgid "Summary"
msgstr ""

#: Avg level
msgctxt "Avg level"
msgid "Avg level"
msgstr ""

#: WTR
msgctxt "WTR"
msgid "WTR"
msgstr ""

#: WN8
msgctxt "WN8"
msgid "WN8"
msgstr ""

#: EFF
msgctxt "EFF"
msgid "EFF"
msgstr ""

#: WGR
msgctxt "WGR"
msgid "WGR"
msgstr ""

#: updated
msgctxt "updated"
msgid "updated"
msgstr ""

#: toWithSpaces
msgctxt "toWithSpaces"
msgid " to "
msgstr ""

#: avg
msgctxt "avg"
msgid "avg"
msgstr ""

#: top
msgctxt "top"
msgid "top"
msgstr ""

#: draws
msgctxt "draws"
msgid "draws"
msgstr ""

#: Maximum damage
msgctxt "Maximum damage"
msgid "Maximum damage"
msgstr ""

#: Specific damage (Avg dmg / HP)
msgctxt "Specific damage (Avg dmg / HP)"
msgid "Specific damage (Avg dmg / HP)"
msgstr ""

#: Capture points
msgctxt "Capture points"
msgid "Capture points"
msgstr ""

#: Defence points
msgctxt "Defence points"
msgid "Defence points"
msgstr ""

#: Filter
msgctxt "Filter"
msgid "Filter"
msgstr ""

#: Extra data (WoT 0.8.8+)
msgctxt "Extra data (WoT 0.8.8+)"
msgid "Extra data (WoT 0.8.8+)"
msgstr ""

#: Average battle time
msgctxt "Average battle time"
msgid "Average battle time"
msgstr ""

#: Average battle time per day
msgctxt "Average battle time per day"
msgid "Average battle time per day"
msgstr ""

#: Battles after 0.8.8
msgctxt "Battles after 0.8.8"
msgid "Battles after 0.8.8"
msgstr ""

#: Average experience
msgctxt "Average experience"
msgid "Average experience"
msgstr ""

#: Average experience without premium
msgctxt "Average experience without premium"
msgid "Average experience without premium"
msgstr ""

#: Average distance driven per battle
msgctxt "Average distance driven per battle"
msgid "Average distance driven per battle"
msgstr ""

#: Average woodcuts per battle
msgctxt "Average woodcuts per battle"
msgid "Average woodcuts per battle"
msgstr ""

#: Average damage assisted
msgctxt "Average damage assisted"
msgid "Average damage assisted"
msgstr ""

#: by tracking
msgctxt "    by tracking"
msgid "    by tracking"
msgstr ""

#: by spotting
msgctxt "    by spotting"
msgid "    by spotting"
msgstr ""

#: Average HE shells fired (splash)
msgctxt "Average HE shells fired (splash)"
msgid "Average HE shells fired (splash)"
msgstr ""

#: Average HE shells received (splash)
msgctxt "Average HE shells received (splash)"
msgid "Average HE shells received (splash)"
msgstr ""

#: Average penetrations per battle
msgctxt "Average penetrations per battle"
msgid "Average penetrations per battle"
msgstr ""

#: Average hits received
msgctxt "Average hits received"
msgid "Average hits received"
msgstr ""

#: Average penetrations received
msgctxt "Average penetrations received"
msgid "Average penetrations received"
msgstr ""

#: Average ricochets received
msgctxt "Average ricochets received"
msgid "Average ricochets received"
msgstr ""

#: better than
msgctxt "better than"
msgid "better than"
msgstr ""

#: PutOwnCrew
msgctxt "PutOwnCrew"
msgid "Put own crew"
msgstr ""

#: PutBestCrew
msgctxt "PutBestCrew"
msgid "Put best crew"
msgstr ""

#: PutClassCrew
msgctxt "PutClassCrew"
msgid "Put same class crew"
msgstr ""

#: PutPreviousCrew
msgctxt "PutPreviousCrew"
msgid "Put previous crew"
msgstr ""

#: DropAllCrew
msgctxt "DropAllCrew"
msgid "Drop all crew"
msgstr ""

#: noSkills
msgctxt "noSkills"
msgid "No skills"
msgstr ""

#: gun_reload_time/actual
msgctxt "gun_reload_time/actual"
msgid "Actual gun reload time"
msgstr ""

#: view_range/base
msgctxt "view_range/base"
msgid "base"
msgstr ""

#: view_range/actual
msgctxt "view_range/actual"
msgid "actual"
msgstr ""

#: view_range/stereoscope
msgctxt "view_range/stereoscope"
msgid "with stereoscope"
msgstr ""

#: radio_range/base
msgctxt "radio_range/base"
msgid "base"
msgstr ""

#: radio_range/actual
msgctxt "radio_range/actual"
msgid "actual"
msgstr ""

#: lowAmmo
msgctxt "lowAmmo"
msgid "Low ammo"
msgstr ""

#: shootingRadius
msgctxt "shootingRadius"
msgid "Shooting radius"
msgstr ""

#: pitchLimitsSide
msgctxt "pitchLimitsSide"
msgid "Elevation arc (side)"
msgstr ""

#: pitchLimitsRear
msgctxt "pitchLimitsRear"
msgid "Elevation arc (rear)"
msgstr ""

#: terrainResistance
msgctxt "terrainResistance"
msgid "Terrain resistance (hard/medium/soft)"
msgstr ""

#: gravity
msgctxt "gravity"
msgid "Gravity"
msgstr ""

#: shellSpeed
msgctxt "shellSpeed"
msgid "Shell speed"
msgstr ""

#: general
msgctxt "general"
msgid "General"
msgstr ""

#: armor
msgctxt "armor"
msgid "Armor"
msgstr ""

#: firepower
msgctxt "firepower"
msgid "Firepower"
msgstr ""

#: mobility
msgctxt "mobility"
msgid "Mobility"
msgstr ""

#: crew
msgctxt "crew"
msgid "Crew"
msgstr ""

#: (m/sec)
msgctxt "(m/sec)"
msgid "(m/sec)"
msgstr ""

#: (sec)
msgctxt "(sec)"
msgid "(sec)"
msgstr ""

#: (m)
msgctxt "(m)"
msgid "(m)"
msgstr ""

#: ussr
msgctxt "ussr"
msgid "USSR"
msgstr ""

#: germany
msgctxt "germany"
msgid "Germany"
msgstr ""

#: usa
msgctxt "usa"
msgid "USA"
msgstr ""

#: france
msgctxt "france"
msgid "France"
msgstr "إلغاء"

#: uk
msgctxt "uk"
msgid "UK"
msgstr ""

#: china
msgctxt "china"
msgid "China"
msgstr ""

#: japan
msgctxt "japan"
msgid "Japan"
msgstr ""

#: czech
msgctxt "czech"
msgid "Czech"
msgstr ""

#: poland
msgctxt "poland"
msgid "Poland"
msgstr ""

#: sweden
msgctxt "sweden"
msgid "Sweden"
msgstr ""

#: italy
msgctxt "italy"
msgid "Italy"
msgstr ""

#: HT
msgctxt "HT"
msgid "HT"
msgstr ""

#: MT
msgctxt "MT"
msgid "MT"
msgstr ""

#: LT
msgctxt "LT"
msgid "LT"
msgstr ""

#: TD
msgctxt "TD"
msgid "TD"
msgstr ""

#: SPG
msgctxt "SPG"
msgid "SPG"
msgstr ""

#: blownUp
msgctxt "blownUp"
msgid "Blown-up!"
msgstr ""

#: ver/currentVersion
msgctxt "ver/currentVersion"
msgid "XVM {0} ({1})"
msgstr ""

#: ver/newVersion
msgctxt "ver/newVersion"
msgid ""
"Update available: <a href='#XVM_SITE_DL#'><font color='#00FF00'>XVM {0}</"
"font></a>\n"
"{1}"
msgstr ""

#: xvmsvc/not_connected
msgctxt "xvmsvc/not_connected"
msgid "<font color='#FFFF00'>no connection to XVM server</font>"
msgstr ""

#: token/services_unavailable
msgctxt "token/services_unavailable"
msgid ""
"Network services unavailable.&nbsp;&nbsp;<a "
"href='#XVM_SITE_UNAVAILABLE#'><font size='11'>more info</font></a>"
msgstr ""

#: token/services_inactive
msgctxt "token/services_inactive"
msgid ""
"Network services inactive.&nbsp;&nbsp;<br><a "
"href='#XVM_SITE_INACTIVE#'><font size='12'>Activate</font></a>&nbsp;&nbsp;"
"&nbsp;<a href='#XVM_CHECK_ACTIVATION#'><font size='12'>Check activation</"
"font></a>"
msgstr ""

#: token/services_not_activated
msgctxt "token/services_not_activated"
msgid ""
"Failed to activate network services. Before checking, activate the services "
"in your account on the XVM website.<br><a href='#XVM_SITE_INACTIVE#'><font "
"size='12'>Activate</font></a>&nbsp;&nbsp;&nbsp;<a "
"href='#XVM_CHECK_ACTIVATION#'><font size='12'>Check activation</font></a>"
msgstr ""

#: token/blocked
msgctxt "token/blocked"
msgid ""
"Status: <font color='#FF0000'>Blocked</font>&nbsp;&nbsp;<a "
"href='#XVM_SITE_BLOCKED#'><font size='11'>more info</font></a>"
msgstr ""

#: token/active
msgctxt "token/active"
msgid "Status:<tab/><font color='#00FF00'>Active</font>"
msgstr ""

#: token/time_left
msgctxt "token/time_left"
msgid "<tab/><font color='#EEEEEE'>{0}d. {1}h. {2}m.</font>"
msgstr ""

#: token/time_left_warn
msgctxt "token/time_left_warn"
msgid ""
"<tab/><font color='#EEEEEE'>{0}d. {1}h. {2}m.</font>\n"
"<tab/><a href='#XVM_SITE#'>Prolong</a>"
msgstr ""

#: token/unknown_status
msgctxt "token/unknown_status"
msgid "Unknown status"
msgstr ""

#: stats_link/svcmsg
msgctxt "stats_link/svcmsg"
msgid ""
"<a href='event:https://stats.modxvm.com/en/stats/players/{0}#svcmsg'><font "
"color='#E2D2A2'>Your stats on the XVM website</font></a>"
msgstr ""

#: stats_link/profile_self
msgctxt "stats_link/profile_self"
msgid ""
"<a href='event:https://stats.modxvm.com/en/stats/players/{0}#profile'><font "
"color='#E2D2A2'><u>Your stats on the XVM website</u></font></a>"
msgstr ""

#: stats_link/profile
msgctxt "stats_link/profile"
msgid ""
"<a href='event:https://stats.modxvm.com/en/stats/players/{0}#profile'><font "
"color='#E2D2A2'><u>Player stats on the XVM website</u></font></a>"
msgstr ""

#: lobby/header/gold_locked_tooltip
msgctxt "lobby/header/gold_locked_tooltip"
msgid "Gold is locked"
msgstr ""

#: lobby/header/gold_unlocked_tooltip
msgctxt "lobby/header/gold_unlocked_tooltip"
msgid "Gold is unlocked"
msgstr ""

#: lobby/header/freexp_locked_tooltip
msgctxt "lobby/header/freexp_locked_tooltip"
msgid "Free XP is locked"
msgstr ""

#: lobby/header/freexp_unlocked_tooltip
msgctxt "lobby/header/freexp_unlocked_tooltip"
msgid "Free XP is unlocked"
msgstr ""

#: lobby/header/crystal_locked_tooltip
msgctxt "lobby/header/crystal_locked_tooltip"
msgid "Bonds are locked"
msgstr ""

#: lobby/header/crystal_unlocked_tooltip
msgctxt "lobby/header/crystal_unlocked_tooltip"
msgid "Bonds are unlocked"
msgstr ""

#: lobby/crew/enable_prev_crew
msgctxt "lobby/crew/enable_prev_crew"
msgid "Automatically return crew"
msgstr ""

#: lobby/crew/enable_prev_crew_tooltip
msgctxt "lobby/crew/enable_prev_crew_tooltip"
msgid ""
"<b><font color='#FDF4CE'>{{l10n:lobby/crew/enable_prev_crew}}</font></b>\n"
"Automatically return the crew that fought\n"
"in this vehicle in the previous battle"
msgstr ""

#: profile/xvm_xte_tooltip
msgctxt "profile/xvm_xte_tooltip"
msgid ""
"<b><font color='#FDF4CE' size='16'>xTE</font></b>\n"
"Per-vehicle efficiency\n"
"More info at <font color='#FDF4CE'>https://modxvm.com/ratings/</font>"
msgstr ""

#: profile/xvm_xte_extended_tooltip
msgctxt "profile/xvm_xte_extended_tooltip"
msgid ""
"<textformat tabstops='[20, 85, 140]'>{{l10n:profile/xvm_xte_tooltip}}\n"
"\n"
"Reference values:\n"
"\t\tdamage\tfrags\n"
"\tCurrent:\t<font color='#FDF4CE' size='14'>{0}</font>\t<font "
"color='#FDF4CE' size='14'>{1}</font>\n"
"\tAverage:\t<font color='#FDF4CE' size='14'>{2}</font>\t<font "
"color='#FDF4CE' size='14'>{3}</font>\n"
"\tTop:\t<font color='#FDF4CE' size='14'>{4}</font>\t<font color='#FDF4CE' "
"size='14'>{5}</font>"
msgstr ""

#: Premium
msgctxt "Premium"
msgid "Premium"
msgstr ""

#: PremiumTooltipHeader
msgctxt "PremiumTooltipHeader"
msgid "Premium vehicles"
msgstr ""

#: PremiumTooltipBody
msgctxt "PremiumTooltipBody"
msgid "Show only premium vehicles."
msgstr ""

#: Special
msgctxt "Special"
msgid "Reward"
msgstr ""

#: SpecialTooltipHeader
msgctxt "SpecialTooltipHeader"
msgid "Reward vehicles"
msgstr ""

#: SpecialTooltipBody
msgctxt "SpecialTooltipBody"
msgid "Show only reward vehicles."
msgstr ""

#: Normal
msgctxt "Normal"
msgid "Normal"
msgstr ""

#: NormalTooltipHeader
msgctxt "NormalTooltipHeader"
msgid "Normal Vehicles"
msgstr ""

#: NormalTooltipBody
msgctxt "NormalTooltipBody"
msgid "Show only normal (non-premium) vehicles."
msgstr ""

#: NonElite
msgctxt "NonElite"
msgid "Non elite"
msgstr ""

#: NonEliteTooltipHeader
msgctxt "NonEliteTooltipHeader"
msgid "Non Elite Vehicles"
msgstr ""

#: NonEliteTooltipBody
msgctxt "NonEliteTooltipBody"
msgid ""
"Show only vehicles with not all modules and technical branches researched."
msgstr ""

#: CompleteCrew
msgctxt "CompleteCrew"
msgid "Complete crew"
msgstr ""

#: CompleteCrewTooltipHeader
msgctxt "CompleteCrewTooltipHeader"
msgid "Complete Crew"
msgstr ""

#: CompleteCrewTooltipBody
msgctxt "CompleteCrewTooltipBody"
msgid "Show only vehicles with complete crew."
msgstr ""

#: TrainingCrew
msgctxt "TrainingCrew"
msgid "Training crew"
msgstr ""

#: TrainingCrewTooltipHeader
msgctxt "TrainingCrewTooltipHeader"
msgid "Training Crew"
msgstr ""

#: TrainingCrewTooltipBody
msgctxt "TrainingCrewTooltipBody"
msgid "Show only vehicles that have crews with less than 100% training level."
msgstr ""

#: NoMaster
msgctxt "NoMaster"
msgid "No master"
msgstr ""

#: NoMasterTooltipHeader
msgctxt "NoMasterTooltipHeader"
msgid "No Master"
msgstr ""

#: NoMasterTooltipBody
msgctxt "NoMasterTooltipBody"
msgid "Show only vehicles without \"Ace Tanker\" mastery badge."
msgstr ""

#: ReserveFilter
msgctxt "ReserveFilter"
msgid "Reserve"
msgstr ""

#: ReserveFilterTooltipHeader
msgctxt "ReserveFilterTooltipHeader"
msgid "Reserve"
msgstr ""

#: ReserveFilterTooltipBody
msgctxt "ReserveFilterTooltipBody"
msgid "Show only reserved vehicles."
msgstr ""

#: check_reserve_menu
msgctxt "check_reserve_menu"
msgid "Set as reserve"
msgstr ""

#: uncheck_reserve_menu
msgctxt "uncheck_reserve_menu"
msgid "Uncheck Reserve label"
msgstr ""

#: reserve_confirm_title
msgctxt "reserve_confirm_title"
msgid "Hide tank"
msgstr ""

#: reserve_confirm_message
msgctxt "reserve_confirm_message"
msgid ""
"Are you sure you want to mark the tank as reserve? It will be available via "
"filters in carousel."
msgstr ""

#: Used slots
msgctxt "Used slots"
msgid "Used slots"
msgstr ""

#: Network services unavailable
msgctxt "Network services unavailable"
msgid "Network services unavailable"
msgstr ""

#: Error loading comments
msgctxt "Error loading comments"
msgid "Error loading comments"
msgstr ""

#: Error saving comments
msgctxt "Error saving comments"
msgid "Error saving comments"
msgstr ""

#: Comments disabled
msgctxt "Comments disabled"
msgid "Comments disabled"
msgstr ""

#: Edit data
msgctxt "Edit data"
msgid "Edit data"
msgstr ""

#: Nick
msgctxt "Nick"
msgid "Nick"
msgstr ""

#: Group
msgctxt "Group"
msgid "Group"
msgstr ""

#: Comment
msgctxt "Comment"
msgid "Comment"
msgstr ""

#: Destroyed
msgctxt "Destroyed"
msgid "Destroyed"
msgstr ""

#: No data
msgctxt "No data"
msgid "No data"
msgstr ""

#: Not ready
msgctxt "Not ready"
msgid "Not ready"
msgstr ""

#: Hide with honors
msgctxt "Hide with honors"
msgid "Hide with honors"
msgstr ""

#: Started
msgctxt "Started"
msgid "Started"
msgstr ""

#: Incomplete
msgctxt "Incomplete"
msgid "Incomplete"
msgstr ""

#: XVM config reloaded
msgctxt "XVM config reloaded"
msgid "XVM config reloaded"
msgstr ""

#: Config file xvm.xc was not found, using the built-in config
msgctxt "Config file xvm.xc was not found, using the built-in config"
msgid "Config file xvm.xc was not found, using the built-in config"
msgstr ""

#: Error loading XVM config
msgctxt "Error loading XVM config"
msgid "Error loading XVM config"
msgstr ""

#: bootcamp_workaround_title
msgctxt "bootcamp_workaround_title"
msgid "Warning"
msgstr "تحذير"

#: bootcamp_workaround_message
msgctxt "bootcamp_workaround_message"
msgid ""
"Currently XVM is not compatible with the \"boot camp\" game mode.\n"
"\n"
"To enter the \"boot camp\" mode, you must restart the World of Tanks client "
"in safe mode, which does not load modifications.\n"
"\n"
"Do you want to restart World of Tanks without mods right now?"
msgstr ""
